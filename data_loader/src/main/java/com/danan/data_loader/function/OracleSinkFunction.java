package com.danan.data_loader.function;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import com.danan.data_loader.common.Database;
import com.danan.data_loader.config.SourceConfig;
import com.danan.data_loader.config.TargetConfig;
import com.danan.data_loader.mapper.TargetMapper;
import com.danan.data_loader.util.ConnectionUtil;
import com.danan.data_loader.util.SqlSessionFactoryUtil;
import com.danan.data_loader.util.metadata.impl.oracle.OracleColumnMetaData;
import com.danan.data_loader.util.metadata.impl.oracle.OracleTableMetaData;
import lombok.extern.slf4j.Slf4j;
import org.apache.flink.configuration.Configuration;
import org.apache.flink.shaded.jackson2.com.fasterxml.jackson.annotation.JsonAlias;
import org.apache.flink.streaming.api.functions.sink.RichSinkFunction;
import org.apache.ibatis.session.SqlSession;
import org.apache.ibatis.session.SqlSessionFactory;


import java.sql.Connection;
import java.sql.SQLOutput;
import java.util.*;
import java.util.stream.Collectors;

/**
 * Created with IntelliJ IDEA.
 *
 * @Author: NanHuang
 * @Date: 2023/05/15/9:11
 * @Description:
 */
@Slf4j
public class OracleSinkFunction extends RichSinkFunction<String> {
    private final SourceConfig sourceConfig;
    private final TargetConfig targetConfig;
    private Map<String, String> schemaMap;
    private final Map<String, List<String>> primaryKeyMap = new HashMap<>();
    private TargetMapper targetMapper;

//    private List<String> sourceTableList = new ArrayList<>();
//    private List<String> targetTableList = new ArrayList<>();
//
//    private Map<String,String> createTableSQL = new HashMap<>();

    public OracleSinkFunction(String sourceConfig, String targetConfig) {
        this.sourceConfig = JSONObject.parseObject(sourceConfig, SourceConfig.class);
        this.targetConfig = JSONObject.parseObject(targetConfig, TargetConfig.class);
    }

    @Override
    public void open(Configuration parameters) throws Exception {
        // 1 Schema映射关系验证
        checkSchemas();
        // 2 获取所有表的主键信息
        getPrimaryKeys();
        // 3 MyBatis初始化
        SqlSessionFactory sqlSessionFactory = SqlSessionFactoryUtil.getSqlSessionFactory();
        SqlSession sqlSession = sqlSessionFactory.openSession(true);
        targetMapper = sqlSession.getMapper(TargetMapper.class);
    }

    @Override
    public void invoke(String value, Context context) throws Exception {
//        System.out.println(value);

        JSONObject obj = JSON.parseObject(value);
        String schema = "";
        if ("oracle".equalsIgnoreCase(targetConfig.getType())) {
            schema = schemaMap.get(obj.getString("schema"));
        } else {
            schema = sourceConfig.getDatabase();
        }
        String table = obj.getString("table");
        String type = obj.getString("type");
        JSONObject after = obj.getJSONObject("after");
        JSONObject before = obj.getJSONObject("before");

        switch (type) {
            case "c":
            case "r":
                log.info(" *** 表{}插入数据 ***", schema + "." + table);
                targetMapper.insert(schema, table, getColumns(after), getValues(after));
                break;
            case "u":
                targetMapper.update(schema, table, getColumnsAndValues(before, schema, table, type), getColumnsAndValues(before, after));
                break;
            case "d":
                targetMapper.delete(schema, table, getColumnsAndValues(before, schema, table, type));
                break;
        }
    }

    /**
     * @Description 过滤出修改的数据
     * @Param [beforeObj, afterObj]
     **/
    private ArrayList<String> getColumnsAndValues(JSONObject beforeObj, JSONObject afterObj) {
        ArrayList<String> beforeColumnsAndValues = getColumnsAndValues(beforeObj);
        ArrayList<String> afterColumnsAndValues = getColumnsAndValues(afterObj);
        afterColumnsAndValues.removeIf(beforeColumnsAndValues::contains);
        return afterColumnsAndValues;
    }

    /**
     * @Description 获取筛选条件
     * @Param [beforeObj, table, schema]
     **/
    private ArrayList<String> getColumnsAndValues(JSONObject beforeObj, String schema, String table, String type) throws Exception {
        List<String> primaryKeys = primaryKeyMap.get(schema + "." + table);
        ArrayList<String> result = new ArrayList<>();
        String op = "u".equals(type) ? "修改" : "删除";
        if (primaryKeys.size() != 0) {
            log.info("表{}根据主键筛选，并{}数据", schema + "." + table, op);
            for (String primaryKey : primaryKeys) {
                result.add(primaryKey + "=" + beforeObj.getString(primaryKey));
            }
        } else {
            List<String> newKeys = getPrimaryKeys(schema, table);
            if (newKeys.size() != 0) {
                log.info("表{}根据主键筛选，并{}数据", schema + "." + table, op);
                primaryKeyMap.put(schema + "." + table, newKeys);
                for (String newKey : newKeys) {
                    result.add(newKey + "=" + beforeObj.getString(newKey));
                }
            } else {
                log.info("表{}根据所有旧数据筛选，并{}数据", schema + "." + table, op);
                result = getColumnsAndValues(beforeObj);
                result.removeIf(s -> s.contains("=null"));
            }
        }
        return result;
    }

    private ArrayList<String> getColumnsAndValues(JSONObject obj) {
        Set<String> columns = obj.keySet();
        ArrayList<String> result = new ArrayList<>();
        for (String column : columns) {
            result.add(column + "=" + obj.getString(column));
        }
        return result;
    }

    private ArrayList<String> getValues(JSONObject obj) {
        Set<String> columns = obj.keySet();
        ArrayList<String> result = new ArrayList<>();
        for (String column : columns) {
            result.add(obj.getString(column));
        }
        return result;
    }

    private ArrayList<String> getColumns(JSONObject obj) {
        return new ArrayList<>(obj.keySet());
    }

    @Override
    public void close() throws Exception {
        super.close();
    }

    /**
     * @Description 获取指定表的主键信息
     * @Param [schema, table]
     **/
    private List<String> getPrimaryKeys(String schema, String table) throws Exception {
        String type = sourceConfig.getType();
        Database DB = Database.valueOf(type.toUpperCase());
        Connection connection = ConnectionUtil.get(
                DB.driverClass,
                String.format(DB.urlTemplate, sourceConfig.getHostname(), sourceConfig.getPort(), sourceConfig.getDatabase()),
                sourceConfig.getUsername(),
                sourceConfig.getPassword()
        );
        List<String> primaryKeys = new OracleColumnMetaData().getPrimaryKeys(connection, schema, table);
        connection.close();
        return primaryKeys;
    }

    /**
     * @Description 获取所有表的主键信息
     * @Param []
     **/
    private void getPrimaryKeys() throws Exception {
        String type = sourceConfig.getType();
        Database DB = Database.valueOf(type.toUpperCase());
        String[] schemaList = sourceConfig.getSchemaList();
        Connection connection = ConnectionUtil.get(
                DB.driverClass,
                String.format(DB.urlTemplate, sourceConfig.getHostname(), sourceConfig.getPort(), sourceConfig.getDatabase()),
                sourceConfig.getUsername(),
                sourceConfig.getPassword()
        );
        for (String schema : schemaList) {
            List<String> tables = new OracleTableMetaData().getMetaData(connection, schema);
            for (String table : tables) {
                primaryKeyMap.put(schema + "." + table, new OracleColumnMetaData().getPrimaryKeys(connection, schema, table));
            }
        }
        connection.close();
    }

    /**
     * @Description 校验源schema列表和目标schema列表是否匹配，并设置映射关系
     * @Param []
     **/
    private void checkSchemas() {
        String[] sourceSchemaList = sourceConfig.getSchemaList();
        String[] targetSchemaList = targetConfig.getSchemaList();
        if (sourceSchemaList.length == 0) {
            throw new RuntimeException("Please set source schema list, this value cannot be empty!");
        }
        if (targetSchemaList.length == 0) {
            targetSchemaList = sourceSchemaList;
            targetConfig.setSchemaList(sourceSchemaList);
        }
        if (sourceSchemaList.length != targetSchemaList.length) {
            throw new RuntimeException("Please set source schema list and target schema list correctly, and the number of schemas must be equal!");
        }

        this.schemaMap = new HashMap<>();
        for (int i = 0; i < sourceSchemaList.length; i++) {
            schemaMap.put(sourceSchemaList[i], targetSchemaList[i]);
        }
    }
}
