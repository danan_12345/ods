package com.danan.data_loader.util;

import java.sql.Connection;
import java.sql.DriverManager;

/**
 * Created with IntelliJ IDEA.
 *
 * @Author: NanHuang
 * @Date: 2023/05/15/15:06
 * @Description:
 */
public class ConnectionUtil {

    public static Connection get(String classDriver, String url, String username, String password) throws Exception {
        Class.forName(classDriver);
        return DriverManager.getConnection(url,username,password);
    }

}
