package com.danan.data_loader.util.metadata.impl.oracle;

import com.danan.data_loader.model.ColumnModel;
import com.danan.data_loader.util.metadata.ColumnMetaData;

import java.sql.Connection;
import java.sql.DatabaseMetaData;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

/**
 * Created with IntelliJ IDEA.
 *
 * @Author: NanHuang
 * @Date: 2023/05/15/16:14
 * @Description:
 */
public class OracleColumnMetaData implements ColumnMetaData<ColumnModel> {
    @Override
    public List<ColumnModel> getMetaData(Connection connection, String schema, String table) {
        try {

            DatabaseMetaData metaData = connection.getMetaData();
            ResultSet resultSet = metaData.getColumns(null, schema, table, "%");
            while (resultSet.next()){
                System.out.println("1 :" + resultSet.getString(1));
                System.out.println("2 :" + resultSet.getString(2));
                System.out.println("3 :" + resultSet.getString(3));
                System.out.println("4 :" + resultSet.getString(4));
                System.out.println("5 :" + resultSet.getString(5));
                System.out.println("6 :" + resultSet.getString(6));
                System.out.println("7 :" + resultSet.getString(7));
                System.out.println("8 :" + resultSet.getString(8));
                System.out.println("9 :" + resultSet.getString(9));
                System.out.println("10:" + resultSet.getString(10));
                System.out.println("11:" + resultSet.getString(11));
                System.out.println("12:" + resultSet.getString(12));
                System.out.println("13:" + resultSet.getString(13));
                System.out.println("14:" + resultSet.getString(14));
                System.out.println("15:" + resultSet.getString(15));
                System.out.println("16:" + resultSet.getString(16));
                System.out.println("17:" + resultSet.getString(17));
                System.out.println("18:" + resultSet.getString(18));
                System.out.println("19:" + resultSet.getString(19));
                System.out.println("20:" + resultSet.getString(20));
                System.out.println("21:" + resultSet.getString(21));
                System.out.println("22:" + resultSet.getString(22));
                System.out.println("23:" + resultSet.getString(23));
                System.out.println("24:" + resultSet.getString(24));
                System.out.println("25:" + resultSet.getString("PRIMARY_KEY"));
                System.out.println("*********************************************************************************");
            }

            return null;

        } catch (SQLException e) {
            throw new RuntimeException(e);
        }
    }

    @Override
    public List<String> getPrimaryKeys(Connection connection, String schema, String table) {
        try {
            DatabaseMetaData metaData = connection.getMetaData();
            ResultSet resultSet = metaData.getPrimaryKeys(null, schema, table);
            ArrayList<String> result = new ArrayList<>();
            while (resultSet.next()) {
                result.add(resultSet.getString("COLUMN_NAME"));
            }
            return result;
        } catch (SQLException e) {
            throw new RuntimeException(e);
        }
    }
}
