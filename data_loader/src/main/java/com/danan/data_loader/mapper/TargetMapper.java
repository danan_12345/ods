package com.danan.data_loader.mapper;

import com.danan.data_loader.model.DataModel;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * Created with IntelliJ IDEA.
 *
 * @Author: NanHuang
 * @Date: 2023/05/16/15:22
 * @Description:
 */
public interface TargetMapper {

    void insert(@Param("schema") String schema, @Param("table") String table, @Param("columns") List<String> columns, @Param("values") List<String> values);

    void update(@Param("schema") String schema, @Param("table") String table, @Param("beforeValues") List<String> beforeValues, @Param("afterValues") List<String> afterValues);

    void delete(@Param("schema") String schema, @Param("table") String table, @Param("beforeValues") List<String> beforeValues);

    void batchInsert(@Param("lines") List<DataModel> lines);

}
