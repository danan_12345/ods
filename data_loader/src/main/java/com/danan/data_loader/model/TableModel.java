package com.danan.data_loader.model;

import lombok.Data;

import java.io.Serializable;
import java.util.List;

/**
 * Created with IntelliJ IDEA.
 *
 * @Author: NanHuang
 * @Date: 2023/05/15/15:25
 * @Description:
 */
@Data
public class TableModel implements Serializable {
    private String tableName;
    private List<String> primaryKeys;
}
