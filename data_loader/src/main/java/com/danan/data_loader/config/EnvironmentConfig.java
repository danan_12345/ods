package com.danan.data_loader.config;

import lombok.Data;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.PropertySource;
import scala.Int;

import java.io.Serializable;

/**
 * Created with IntelliJ IDEA.
 *
 * @Author: NanHuang
 * @Date: 2023/05/10/9:56
 * @Description: 编程环境配置
 */
@Data
@Configuration
@PropertySource("file:${user.dir}/application.yml")
//@PropertySource("classpath:application.yml")
@ConfigurationProperties(prefix = "environment")
public class EnvironmentConfig {
    private String checkpointStorage;
    private Integer parallelism;
    private String cdcModel;
    private Integer port;
}
