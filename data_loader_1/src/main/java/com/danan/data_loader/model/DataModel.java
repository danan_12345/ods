package com.danan.data_loader.model;

import lombok.AllArgsConstructor;
import lombok.Data;

import java.io.Serializable;

/**
 * Created with IntelliJ IDEA.
 *
 * @Author: NanHuang
 * @Date: 2023/05/25/11:52
 * @Description:
 */
@Data
@AllArgsConstructor
public class DataModel implements Serializable {
    private String schema;
    private String table;
    private String columns;
    private String values;
}
