package com.danan.data_loader.util;

import org.apache.ibatis.io.Resources;
import org.apache.ibatis.session.SqlSessionFactory;
import org.apache.ibatis.session.SqlSessionFactoryBuilder;

import java.io.IOException;
import java.io.InputStream;

public class SqlSessionFactoryUtil {
    public static SqlSessionFactory getSqlSessionFactory() {
        InputStream rs = null;
        SqlSessionFactory ssf = null;
        try {
            // 1 读取MyBatis的核心配置文件
            rs = Resources.getResourceAsStream("mybatis-config.xml");
            // 2 创建SqlSessionFactoryBuilder对象
            SqlSessionFactoryBuilder sfb = new SqlSessionFactoryBuilder();
            // 3 通过核心配置文件对象创建工厂类SqlSessionFactory，用于生产SqlSession对象
            ssf = sfb.build(rs);
        } catch (IOException e) {
            throw new RuntimeException(e);
        } finally {
            if (rs != null) {
                try {
                    rs.close();
                } catch (IOException e) {
                    throw new RuntimeException(e);
                }
            }
        }
        return ssf;
    }
}
