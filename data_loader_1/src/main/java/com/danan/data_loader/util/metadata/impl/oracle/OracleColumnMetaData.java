package com.danan.data_loader.util.metadata.impl.oracle;

import com.danan.data_loader.model.ColumnModel;
import com.danan.data_loader.util.metadata.ColumnMetaData;

import java.sql.Connection;
import java.sql.DatabaseMetaData;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

/**
 * Created with IntelliJ IDEA.
 *
 * @Author: NanHuang
 * @Date: 2023/05/15/16:14
 * @Description:
 */
public class OracleColumnMetaData implements ColumnMetaData<ColumnModel> {

    @Override
    public List<String> getPrimaryKeys(Connection connection, String schema, String table) {
        try {
            DatabaseMetaData metaData = connection.getMetaData();
            ResultSet resultSet = metaData.getPrimaryKeys(null, schema, table);
            ArrayList<String> result = new ArrayList<>();
            while (resultSet.next()) {
                result.add(resultSet.getString("COLUMN_NAME"));
            }
            return result;
        } catch (SQLException e) {
            throw new RuntimeException(e);
        }
    }
}
