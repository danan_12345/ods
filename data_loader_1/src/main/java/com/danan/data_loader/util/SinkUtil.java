package com.danan.data_loader.util;

import com.danan.data_loader.function.OracleSinkFunction;

/**
 * Created with IntelliJ IDEA.
 *
 * @Author: NanHuang
 * @Date: 2023/05/29/16:40
 * @Description:
 */
public class SinkUtil {

    public static OracleSinkFunction getOracleSinkFunction(String schema){
        return new OracleSinkFunction(schema);
    }

}
