package com.danan.data_loader.util.metadata.impl.oracle;

import com.danan.data_loader.util.metadata.TableMetaData;

import java.sql.Connection;
import java.sql.DatabaseMetaData;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

/**
 * Created with IntelliJ IDEA.
 *
 * @Author: NanHuang
 * @Date: 2023/05/15/15:18
 * @Description:
 */
public class OracleTableMetaData implements TableMetaData<String> {

    @Override
    public List<String> getMetaData(Connection connection, String schema) {
        try {

            DatabaseMetaData metaData = connection.getMetaData();
            ResultSet resultSet = metaData.getTables(null, schema, null, new String[]{"TABLE"});
            ArrayList<String> result = new ArrayList<>();
            while (resultSet.next()){
                result.add(resultSet.getString("TABLE_NAME"));
            }
            return result;

        } catch (SQLException e) {
            throw new RuntimeException(e);
        }
    }

}
