package com.danan.data_loader.util.metadata;


import java.sql.Connection;
import java.util.List;

/**
 * Created with IntelliJ IDEA.
 *
 * @Author: NanHuang
 * @Date: 2023/05/15/15:14
 * @Description:
 */
public interface ColumnMetaData<T> {

    List<String> getPrimaryKeys(Connection connection,String schema,String table);

}
