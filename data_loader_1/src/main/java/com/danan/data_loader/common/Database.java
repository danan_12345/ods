package com.danan.data_loader.common;

/**
 * Created with IntelliJ IDEA.
 *
 * @Author: NanHuang
 * @Date: 2023/05/10/10:34
 * @Description: JDBC连接信息
 */
public enum Database {
    MYSQL("com.mysql.cj.jdbc.Driver","jdbc:mysql://%s:%s/%s?useSSL=false",3306,64),
    ORACLE("oracle.jdbc.driver.OracleDriver","jdbc:oracle:thin:@//%s:%s/%s",1521,30),
    SQLSERVER("com.microsoft.sqlserver.jdbc.SQLServerDriver","jdbc:sqlserver://%s:%s;databaseName=%s;encrypt=false",1433,128);

    public final String driverClass;
    public final String urlTemplate;
    public final Integer defaultPort;
    public final Integer maxTableNameLen;

    Database(String className, String urlTemplate, Integer defaultPort, Integer maxTableNameLen) {
        this.driverClass = className;
        this.urlTemplate = urlTemplate;
        this.defaultPort = defaultPort;
        this.maxTableNameLen = maxTableNameLen;
    }
}
