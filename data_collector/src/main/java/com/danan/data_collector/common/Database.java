package com.danan.data_collector.common;

/**
 * Created with IntelliJ IDEA.
 *
 * @Author: NanHuang
 * @Date: 2023/05/10/10:34
 * @Description: JDBC连接信息
 */
public enum Database {
    MYSQL("com.mysql.cj.jdbc.Driver","jdbc:mysql://{0}:{1,number,#}/{2}?useSSL=false",3306,64),
    ORACLE("oracle.jdbc.driver.OracleDriver","jdbc:oracle:thin:@{0}:{1,number,#}:{2}",1521,30),
    SQLSERVER("com.microsoft.sqlserver.jdbc.SQLServerDriver","jdbc:sqlserver://{0}:{1,number,#};databaseName={2};encrypt=false",1433,128);

    public final String className;
    public final String urlTemplate;
    public final Integer defaultPort;
    public final Integer maxTableNameLen;

    Database(String className, String urlTemplate, Integer defaultPort, Integer maxTableNameLen) {
        this.className = className;
        this.urlTemplate = urlTemplate;
        this.defaultPort = defaultPort;
        this.maxTableNameLen = maxTableNameLen;
    }
}
