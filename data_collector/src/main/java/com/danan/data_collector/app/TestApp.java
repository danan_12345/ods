package com.danan.data_collector.app;

import com.danan.data_collector.config.EnvironmentConfig;
import com.danan.data_collector.config.KafkaConfig;
import com.danan.data_collector.config.SourceConfig;
import com.danan.data_collector.config.TargetConfig;
import com.danan.data_collector.util.MyCdcDeserialization;
import com.ververica.cdc.connectors.mysql.source.MySqlSource;
import com.ververica.cdc.connectors.mysql.table.StartupOptions;
import com.ververica.cdc.connectors.oracle.OracleSource;
import com.ververica.cdc.connectors.sqlserver.SqlServerSource;
import com.ververica.cdc.debezium.DebeziumSourceFunction;
import org.apache.flink.api.common.eventtime.WatermarkStrategy;
import org.apache.flink.api.common.restartstrategy.RestartStrategies;
import org.apache.flink.api.common.serialization.SimpleStringSchema;
import org.apache.flink.configuration.Configuration;
import org.apache.flink.connector.base.DeliveryGuarantee;
import org.apache.flink.connector.kafka.sink.KafkaRecordSerializationSchema;
import org.apache.flink.connector.kafka.sink.KafkaSink;
import org.apache.flink.streaming.api.CheckpointingMode;
import org.apache.flink.streaming.api.datastream.DataStreamSource;
import org.apache.flink.streaming.api.environment.CheckpointConfig;
import org.apache.flink.streaming.api.environment.StreamExecutionEnvironment;
import org.apache.kafka.clients.producer.ProducerConfig;
import org.springframework.boot.CommandLineRunner;
import org.springframework.stereotype.Component;

import javax.annotation.Resource;
import java.util.Arrays;
import java.util.Properties;
import java.util.UUID;
import java.util.stream.Collectors;

/**
 * Created with IntelliJ IDEA.
 *
 * @Author: NanHuang
 * @Date: 2023/05/12/11:37
 * @Description:
 */
@Component
public class TestApp implements CommandLineRunner {
    @Resource
    private StreamExecutionEnvironment env;
    @Resource
    private KafkaSink<String> kafkaSink;
    @Resource
    private DataStreamSource<String> ds;


    @Override
    public void run(String... args) throws Exception {
        ds.sinkTo(kafkaSink);
        env.execute();
    }
}
