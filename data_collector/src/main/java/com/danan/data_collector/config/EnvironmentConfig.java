package com.danan.data_collector.config;

import lombok.Data;
import lombok.Getter;
import lombok.Setter;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.PropertySource;

/**
 * Created with IntelliJ IDEA.
 *
 * @Author: NanHuang
 * @Date: 2023/05/10/9:56
 * @Description: 编程环境配置
 */
@Data
@Configuration
@PropertySource("file:${user.dir}/application.yml")
//@PropertySource("classpath:application.yml")
@ConfigurationProperties(prefix = "environment")
public class EnvironmentConfig {
    private String checkpointStorage1;
    private String checkpointStorage2;
    private Integer parallelism;
    private String cdcModel;
}
