package com.danan.data_collector.config;

import lombok.Data;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.PropertySource;

/**
 * Created with IntelliJ IDEA.
 *
 * @Author: NanHuang
 * @Date: 2023/05/10/10:23
 * @Description: 目标数据库配置
 */
@Data
@Configuration
@PropertySource("file:${user.dir}/application.yml")
//@PropertySource("classpath:application.yml")
@ConfigurationProperties(prefix = "target")
public class TargetConfig {
    private String type;
    private String hostname;
    private Integer port;
    private String username;
    private String password;
    private String database;
    private String[] schemaList;
}
