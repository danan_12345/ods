package com.danan.data_collector.config;

import lombok.Data;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.PropertySource;

/**
 * Created with IntelliJ IDEA.
 *
 * @Author: NanHuang
 * @Date: 2023/05/10/10:21
 * @Description: Kafka配置
 */
@Data
@Configuration
@PropertySource("file:${user.dir}/application.yml")
//@PropertySource("classpath:application.yml")
@ConfigurationProperties(prefix = "kafka")
public class KafkaConfig {
    private String bootstrapServer;
    private String odsTopicName;
    private String consumerGroupId;
    private String consumptionModel;
}
