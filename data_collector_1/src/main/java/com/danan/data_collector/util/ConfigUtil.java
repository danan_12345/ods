package com.danan.data_collector.util;

import java.util.ResourceBundle;

/**
 * @author NanHuang
 * @Date 2023/1/24
 */
public class ConfigUtil {

    private static final ResourceBundle config = ResourceBundle.getBundle("config");

    public static String getProperty(String keyName){
        return config.getString(keyName);
    }

}
